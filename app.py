from data_base import readRoute, readFile, select_chain, timesdef, basic_data, bearings, schedule_file
from querys import insert_empoyees
from bearings import init_brands
import argparse
import logging
logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)
# 'http://localhost/data/'
# 'DllBuf_'
def create_app(route: str, coincidence: str):
    logger.info('Inicio de la aplicación leyendo la ruta {}'.format(route))

    files = readRoute(url=route, coincidence=coincidence)
    for file in files:

        logger.info('schedule file: {}'.format(file))    
        schedule_file(file)   

        logger.info('Creando dataframe: {}'.format(file))
        df = readFile(url=route,name=file)

        logger.info('Seleccionando cadena: {}'.format(file))
        df = select_chain(df=df)

        logger.info('Transformando el tiempo: {}'.format(file))
        df = timesdef(df=df)

        logger.info('Recuperando datos básicos del empleado: {}'.format(file))
        df = basic_data(df=df)

        logger.info('Deternimar AM/PM: {}'.format(file))
        df = bearings(df=df)

        logger.info('Insertando empleados encontrados: {}'.format(file))
        employees = insert_empoyees(df=df)
        logger.info("Empleados insertados:{} ".format(employees))

        logger.info('Leyendo y transformando marcaciones: {}'.format(file))
        init_brands(df)
        logger.info("Fin de marcaciones:{} ".format(employees))




if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('route',
                        help='Ruta donde se encuentran los archivos',
                        type=str)
    parser.add_argument('coincidence',
                        help='Formato del archivo a buscar',
                        type=str)
    args = parser.parse_args()

    create_app(route=args.route, coincidence=args.coincidence)