from datetime import datetime, date, time, timedelta
import itertools
from querys import select_brands, insert_brands

def combinations(any_list):
    return itertools.chain.from_iterable(
itertools.combinations(any_list, i + 1)
        for i in range(len(any_list)))

def transform(df: object):
    
    return [dict(t) for t in set([tuple(d.items()) for d in [ {'time':df.iloc[x].hora,'hh':df.iloc[x].hh,'mm':df.iloc[x].mm} for x in range(len(df))]])]

def compare_time(time1: str, time2: str):
    _time1 = time1.split(':')
    _time2 = time2.split(':')
    
    hour1 = time(int(_time1[0]), int(_time1[1]),int(_time1[2])) 
    hour2 = time(int(_time2[0]), int(_time2[1]),int(_time2[2])) 
    
    if hour1 > hour2:
        return time1, time2
    else:
        return time2, time1
        
def hour_start_end(times: list): 
    h_end=[]
    h_start=[]
    
    for x in times:
        time_end, time_start = compare_time(time1=x[0], time2=x[1])
        h_end.append(time_end)
        h_start.append(time_start)
    h_end= sorted(set(h_end),reverse=False)
    h_start = sorted(set(h_start),reverse=False)
    if h_start[0][0:1] == '0':
        return h_start[0], h_start[1] 
    else:
        return h_end.pop(), h_start[0] 
    
def check(times: dict):
    marcaciones = {}
    listHour =[]
    compare = []
    hour_start = None
    hour_end = None 
    if len(times) == 1:
        hour_start = times[0].get('time')
        hour_end   = '00:00:00'
    elif len(times) == 2:
        end, start = compare_time(time1=times[0].get('time'), time2=times[1].get('time'))
        if start[0:1] == '0':
            hour_start = end
            hour_end = start
        else:
            hour_start = start
            hour_end = end
    else:
        for time in times:
            listHour.append(time.get('time'))
        for x in [list(l) for l in combinations(listHour)]:
            if len(x) == 2:
                compare.append(x)
        hour_end, hour_start = hour_start_end(compare)
    
    marcaciones.update({'hour_start':hour_start})
    marcaciones.update({'hour_end':hour_end})

    return marcaciones

def init_brands(df: object):
    view = []
    marcaciones = []
    for idx, row in df.iterrows():
        rut = int(df.loc[idx,['rut']].item())
        # consulto si ya ese rut fue procesado
        if rut not in view:
            dat = df[df['rut'] == rut]
            times = transform(dat)
            marcado = check(times)
            marcado.update({'rut':rut})
            marcado.update({'fecha':df.loc[idx,['fecha']].item()})
            existe = select_brands(ff=marcado.get('fecha'), rut=marcado.get('rut'))
            if existe is 0:
                insert_brands(data=marcado)
            view.append(rut)
    return marcaciones