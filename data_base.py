import requests
from bs4 import BeautifulSoup
import pandas as pd
import re
from querys import count_file_schedule, insert_file_schedule, update_file_schedule

import logging
logging.basicConfig(level=logging.INFO)
import sys

logger = logging.getLogger(__name__)


def schedule_file(file: str):
    fecha = "{}-{}-{}".format(file[7:9],file[9:11],file[11:15])
    file_schedule = count_file_schedule(fecha,file)

    if file_schedule is None:
        insert_file_schedule({'fecha':fecha, 'file':file, 'iterations': 1})
    else:
        iterations = file_schedule[3]
        iterations+=1
        update_file_schedule(iterations, fecha, file)


def request(url: str):
        try:
                req = requests.get(url)
                status_code = req.status_code
                return req
        except requests.ConnectionError as e:
                 logger.error('SERVIDOR NO DISPONIBLE: {}'.format(url))
                 sys.exit()



def readRoute(url: str, coincidence: str):
    req= request(url)
    
    if req.status_code == 200:
        html = BeautifulSoup(req.text, "html.parser")
        entradas = html.find_all('a')
        
        files = []
        for i, entrada in enumerate(entradas):
            if entrada.getText()[0:7] == coincidence:
                files.append(entrada.getText())
        return files


def readFile(url: str, name: str):
    dataframe = pd.read_csv("{}{}".format(url, name), delimiter=",", encoding="latin-1", 
                        index_col=False ,names=['codigo','fecha','hora','identificador','identificador1','data1',
                                                 'data2','data3','data4','data5','data6','data7','data8','person'])
    return dataframe


def select_chain(df: object):
    chain = (df
             .apply(lambda row: row['person'].find('JUMBO'), axis=1)
            )
    df['chain'] = chain
    df = df[df['chain'] != -1]
    df = df.drop(columns=['chain','identificador','identificador1','data1','data2',
                          'data3','data4','data5','data6','data7','data8'])
    return df 



def timesdef(df: object):
    hora = (df
            .apply(lambda row: row['hora'], axis=1)
            .apply(lambda hora: hora[0:2] if len(hora) == 8 else hora[0:1])
           )
    
    minutos = (df
            .apply(lambda row: row['hora'], axis=1)
            .apply(lambda minutos: minutos[3:5] if len(minutos) == 8 else minutos[2:4])
           )
    
    segundos = (df
            .apply(lambda row: row['hora'], axis=1)
            .apply(lambda ss: ss[6:9] if len(ss) == 8 else ss[5:8])
           )
    df['hh'] = hora
    df['mm'] = minutos
    df['ss'] = segundos
    return df


def basic_data(df: object):
    rut = (df
            .apply(lambda row: int(re.findall("\d+", row['person'])[0]) , axis=1)
           )
    
    name = (df
            .apply(lambda row: re.findall("\D+", row['person'])[0].replace('JUMBO SU','') , axis=1)
           )
    df['rut'] = rut
    df['name'] = name
    return df


def bearings(df: object):
    AmPm = (df
            .apply(lambda row: int(row['hh']), axis=1)
            .apply(lambda hh: 'PM' if hh >= 12  else 'AM' if hh != 0 else 'PM')
    )
    
    df['turno'] = AmPm
    return df
    


