import os

from dotenv import load_dotenv, find_dotenv


class EnvironmentConfig():
    load_dotenv(find_dotenv())
    DATABASEURL = os.environ.get('DATABASEURL')
    PSQL_HOST = os.environ.get('PSQL_HOST')
    PSQL_PORT = os.environ.get('PSQL_PORT')
    PSQL_USER = os.environ.get('PSQL_USER')
    PSQL_PASS = os.environ.get('PSQL_PASS')
    PSQL_DB = os.environ.get('PSQL_DB')