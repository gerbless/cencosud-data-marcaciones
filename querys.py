from enviroment_config import EnvironmentConfig
import psycopg2
import logging
logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)

conn = psycopg2.connect("host={} port={} user={} password={} dbname={}".format(EnvironmentConfig.PSQL_HOST, EnvironmentConfig.PSQL_PORT, EnvironmentConfig.PSQL_USER, 
                                                                        EnvironmentConfig.PSQL_PASS, EnvironmentConfig.PSQL_DB))

def insert_empoyees(df: object):
    num_register= len(df)
    for idx, row in df.iterrows():  
        with conn, conn.cursor() as cur:  
            try:
                cur.execute("INSERT INTO public.employees (rut, nombres, chain) VALUES(%s, %s, %s)", 
                            (int(df.loc[idx,['rut']].item()),str(df.loc[idx,['name']].item()), 'JUMBO'))
            except psycopg2.Error as e:
                num_register-=1
    return num_register
 
def select_brands(ff: str, rut: int):
    sql="SELECT COUNT(*) FROM public.bearings WHERE ff_bearing =to_date('{}','dd/mm/yyyy')  AND id_employees={}".format(ff, rut)
    with conn, conn.cursor() as cur:
        try:
            cur.execute(sql)
            bearings = cur.fetchone() 
            return bearings[0]
        except psycopg2.Error as e:
            logger.error("Err al consultar public.bearings: {}".format(e))
            
def insert_brands(data: dict):
    with conn, conn.cursor() as cur:  
        try:
            cur.execute("INSERT INTO public.bearings (ff_bearing, hh_start, hh_end,id_employees) VALUES(%s, %s, %s, %s)", 
                            (data.get('fecha'),data.get('hour_start'),data.get('hour_end'),data.get('rut')))
        except psycopg2.Error as e:
            logger.error("Err al insertar public.bearings: {}".format(e))

def count_file_schedule(ff: str, files: str):
    with conn, conn.cursor() as cur:
        try:
            cur.execute("SELECT * FROM  public.schedule WHERE fecha=to_date('{}','dd/mm/yyyy') AND file='{}'".format(ff, files))
            schedule = cur.fetchone() 
            return schedule
        except psycopg2.Error as e:
            logger.error("Err al consultar schedule: {}".format(e))

def insert_file_schedule(data: dict):
    with conn, conn.cursor() as cur:  
        try:
            cur.execute("INSERT INTO public.schedule (fecha, file, status,iterations) VALUES(%s, %s, %s, %s)", 
                            (data.get('fecha'),data.get('file'),'true',data.get('iterations')))
        except psycopg2.Error as e:
            logger.error("Err al insertar public.schedule: {}".format(e))
            
def update_file_schedule(iterations: int, ff: str, files: str):
    with conn, conn.cursor() as cur:
        try:
            cur.execute("UPDATE public.schedule SET iterations={} WHERE fecha=to_date('{}','dd/mm/yyyy') AND file='{}'".format(iterations, ff, files))
        except psycopg2.Error as e:
            logger.error("Err update_file_schedule schedule: {}".format(e))
