beautifulsoup4==4.7.1
numpy==1.16.2
pandas==0.24.2
psycopg2==2.8.2
python-dotenv==0.10.3
